/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lynxdb.lynxcore.test;

import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;

import org.lynxdb.lynxcore.exceptions.NotValidClassException;
import org.lynxdb.lynxlist.structure.LynxList;
import org.lynxdb.storerengine.store.StoreManager;
import org.lynxdb.lynxcore.model.Database;
import org.lynxdb.lynxcore.model.Table;

/**
 *
 * @author martin
 */
public class TestMain {
    static long ti, tf;
    
    public static void main(String[] args) throws IOException, ClassNotFoundException, NotValidClassException {
        //execAddComparator();
        execSearchTest();
        System.out.print("Limit: ");
        int lim = new java.util.Scanner(System.in).nextInt();
        
        start();
        Database dbPersonas;
        dbPersonas = new Database("dbx");
        finish();
        printMsg("Cargar bd");
        
        start();
        if (!dbPersonas.hasTable("persona"))
            dbPersonas.createTable("persona", Persona.class);
        
        finish();
        printMsg("Crear tabla");
        
        Persona pers = new Persona(0, "nom0");
        String tblName = "persona";
        start();
        for (int i = 0; i < lim; i++)
            dbPersonas.insertInto(tblName, pers);
        
        finish();
        printMsg("insert de "+lim+" elementos");

        // Problemas aqui
        /*System.out.println("Max: "+dbPersonas.selectMaxFrom("persona", "edad"));
        System.out.println("Min: "+dbPersonas.selectMinFrom("persona", "edad"));
        System.out.println("Sum: "+dbPersonas.selectSumFrom("persona", "edad"));
        System.out.println("Avg: "+dbPersonas.selectAvgFrom("persona", "edad"));*/

//        Cursor cursor = dbPersonas.getCursorFrom("persona");
//        while (cursor.hasNext()) {
//            System.out.println(cursor.next());
//        }

        Persona selectFirstFrom = (Persona) dbPersonas.selectFirstFrom("persona");
        System.out.println("Select first: "+selectFirstFrom);
        Persona first = (Persona) dbPersonas.selectFirstFrom("persona", "name", "nom4");
        System.out.println("First search: "+first);

        start();
        LynxList<Persona> list = dbPersonas.selectAllFrom("persona");
        for (Persona p : list) {}
        finish();
        printMsg("selectAllFrom");

        // Problemas aqui
        //long selectMaxFrom = dbPersonas.selectMaxFrom("persona", "name");
        
        start();
        dbPersonas.dropTable("persona");
        finish();
        printMsg("drop table");

        start();
        dbPersonas.drop();
        finish();
        printMsg("drop database");

    }

    public static long currentTime(){
        return System.currentTimeMillis();
    }
    
    public static void start(){
        ti = currentTime();
    }
    
    public static void finish(){
        tf = currentTime();
        tf-=ti;
    }
    
    public static void printMsg(String msg){
        System.err.println(msg+": "+tf);
    }

    private static void execSearchTest() throws NotValidClassException {
        Database db = new Database("dbPersonas");
        if (!db.hasTable("persona"))
            db.createTable("persona", Persona.class);
        for (int i = 0; i < 10; i++)
            db.insertInto("persona", new Persona(i, "nom"+i));
        System.out.println(db.selectFirstFrom("persona", "edad", 4));
        System.exit(0);
    }

    private static void execTestProblemaLista() throws IOException {
        Database db = new Database("dbPersonas");
    
        Table<Persona> tbl1 = db.getTable("persona");
        
        tbl1.insert(new Persona(1, "nom1"));
        tbl1.insert(new Persona(2, "nom2"));
        tbl1.insert(new Persona(3, "nom3"));
        
        Table tbl2 = db.getTable("persona");
        
//        LynxList<Persona> selectAll = db.selectAllFrom("persona");
//        LynxList<Persona> selectTblGen = tbl1.selectAll();
//        LynxList selectTblO = tbl2.selectAll();
//        LynxList<Persona> objectsStoreGen = tbl1.getStoreManager().getObjects();
//        LynxList<Persona> objectsStoreO = tbl2.getStoreManager().getObjects();
//        StoreManager<Persona> store = new StoreManager<>(Persona.class, new File(db.getStorePath(), "persona"));
//        
//        System.out.println("ListDB: "+selectAll);
//        System.out.println("ListTblGeneric: "+selectTblGen);
//        System.out.println("ListTblObject: "+selectTblO);
//        System.out.println("ListStoreGen: "+objectsStoreGen);
//        System.out.println("ListStoreObject: "+objectsStoreO);
//        System.out.println("ListStorePuro: "+store.getObjects());
        Persona selectFirst = (Persona) db.selectFirstFrom("persona", "name", "nom1");
    }
    
    private static void execAddComparator() throws IOException, NotValidClassException {
        final String TBL_NAME = "persona";
        System.out.print("Limit: ");
        int lim = new java.util.Scanner(System.in).nextInt();
        
        Database db = new Database("db2");
        if(!db.hasTable(TBL_NAME))
            db.createTable(TBL_NAME, Persona.class);
            
        StoreManager<Persona> store = db.getTable(TBL_NAME).getStoreManager();
        
        start();
        for (int i = 0; i < lim; i++) {
            store.addObject(new Persona(i, "nom"+i));
        }
        finish();
        printMsg("addNormal");
        
        store.deleteAllObjects();
        store.startSaver();
        
        start();
        for (int i = 0; i < lim; i++) {
            store.addObject(new Persona(i, "nom"+i));
        }
        finish();
        printMsg("addParallel");
        System.out.println("Cantidad de elementos: "+store.getObjectsCount());
 
        store.deleteAllObjects();
        System.exit(0);
    }
    
}
