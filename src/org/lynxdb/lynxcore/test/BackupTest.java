package org.lynxdb.lynxcore.test;

import org.lynxdb.lynxcore.backup.BackupManager;
import org.lynxdb.lynxcore.exceptions.NotValidClassException;
import org.lynxdb.lynxcore.model.Database;

import java.io.IOException;
import java.util.LinkedList;

public class BackupTest {
    public static void main(String[] args) throws NotValidClassException, IOException, ClassNotFoundException {
        String tbl1 = "persona5";
        String tbl2 = "persona6";
        Database database = new Database("dbTest");
        database.dropAllTables();
        database.createTableIfNotExists(tbl1, Persona.class);
        database.createTableIfNotExists(tbl2, Persona.class);

        LinkedList<Persona> listPersonas = new LinkedList<>();
        for (int i = 0; i < 1000; i++)
            listPersonas.add(new Persona(i, "nom"+i));

        database.insertInto(tbl1, listPersonas);
        database.insertInto(tbl2, listPersonas);
        BackupManager.executeBackup(database);
        database.drop();
        database = null;
        database = BackupManager.restoreBackup("dbTest");

    }
}
