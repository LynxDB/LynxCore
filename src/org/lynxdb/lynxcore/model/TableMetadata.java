/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lynxdb.lynxcore.model;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author martin
 */
public class TableMetadata {
    private File fileMetadata;
    private Properties props;
    
    // Ver si es mas rapido usar metodos nativos en vez de properties en el
    // metadata.
    public TableMetadata(File tblFolder) {
        fileMetadata = new File(tblFolder, "metadata.properties");
        props = new Properties();
        if (fileMetadata.exists())
            loadFile();
        
        else
            saveFile();
    }

    public String getTableName(){
        return props.getProperty("tblName");
    }
    
    public void setTableName(String name){
        props.setProperty("tblName", name);
        saveFile();
    }
    
    public Class<?> getTableClass(){
        try {
            return Class.forName(props.getProperty("tblClass"));
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TableMetadata.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void setTableClass(Class<?> clazz){
        props.setProperty("tblClass", clazz.getName());
        saveFile();
    }
    
    public void deleteFile(){
        fileMetadata.delete();
    }

    // Solo se debe cambiar a donde apunta el puntero porque fisicamente
    // los archivos ya estan en la nueva ruta
//    public void setParentFolder(String parentPath) throws IOException{
//        File newPath = new File(parentPath, fileMetadata.getName());
////        newPath.createNewFile();
////        Files.move(fileMetadata.toPath(), newPath.toPath(), StandardCopyOption.REPLACE_EXISTING);
//        fileMetadata = new File(parentPath, fileMetadata.getName());
//        
//    }
    
    private void saveFile() {
        try {
            props.store(new FileWriter(fileMetadata), "Metadatos");
        } catch (IOException ex) {
            Logger.getLogger(TableMetadata.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void loadFile() {
        try {
            props.load(new FileReader(fileMetadata));
        } catch (IOException ex) {
            Logger.getLogger(TableMetadata.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
