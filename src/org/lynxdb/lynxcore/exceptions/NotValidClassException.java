package org.lynxdb.lynxcore.exceptions;

/**
 *
 * @author martin
 */
public class NotValidClassException extends Exception {

    public NotValidClassException() {}

    public NotValidClassException(String message) {
        super("class "+message+" is not Serializable");
    }
    
}
