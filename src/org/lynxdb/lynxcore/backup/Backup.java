package org.lynxdb.lynxcore.backup;

import org.lynxdb.lynxlist.structure.LynxList;

// es la base de datos respaldada
public class Backup {
    private String database;
    private LynxList<BackupCollection<?>> listCollections;

    public Backup() {}

    public Backup(String database) {
        this.database = database;
    }

    public Backup(String database,
                  LynxList<BackupCollection<?>> listCollections) {
        this.database = database;
        this.listCollections = listCollections;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public LynxList<BackupCollection<?>> getListCollections() {
        return listCollections;
    }

    public void setListCollections(LynxList<BackupCollection<?>> listCollections) {
        this.listCollections = listCollections;
    }

}
