package org.lynxdb.lynxcore.backup;

import org.lynxdb.lynxcore.model.Table;
import org.lynxdb.lynxlist.structure.LynxList;

// es la tabla respaldada
public class BackupCollection<T> {
    private String tableName;
    private String className;
    private LynxList<T> listData;

    public BackupCollection() {}

    public BackupCollection(Table table) {
        tableName = table.getName();
        className = table.getObjectClazz().getName();
        listData = table.selectAll();
        System.out.println("ListDataSize: "+listData.size());
    }

    public String getTableName() {
        return tableName;
    }

    public String getClassName() {
        return className;
    }

    public LynxList<T> getListData() {
        return listData;
    }

}
