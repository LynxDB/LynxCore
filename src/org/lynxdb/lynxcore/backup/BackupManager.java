package org.lynxdb.lynxcore.backup;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import org.lynxdb.lynxcore.exceptions.NotValidClassException;
import org.lynxdb.lynxcore.model.Database;
import org.lynxdb.lynxcore.model.Table;
import org.lynxdb.lynxlist.structure.LynxList;
import org.lynxdb.storerengine.system.SysInfo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;

import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;
import static java.nio.file.StandardOpenOption.WRITE;

public class BackupManager {
    public static void executeBackup(Database db) throws IOException {
        LynxList<Table> listTables = db.getTables();
        Backup backup = new Backup(db.getName());
        LynxList<BackupCollection<?>> listCollections = new LynxList<>();

        for (Table table : listTables)
            listCollections.add(new BackupCollection<>(table));

        backup.setListCollections(listCollections);
        File fileBackup = new File(SysInfo.ROOT_DIR,
                db.getName()+".backup");
        fileBackup.createNewFile();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(backup);
        Files.write(fileBackup.toPath(), json.getBytes(), TRUNCATE_EXISTING);
        System.err.println("BackupPath: "+fileBackup.getCanonicalPath());
    }

    public static Database restoreBackup(String dbName) throws IOException,
            ClassNotFoundException, NotValidClassException {
        return restoreBackup(new File(SysInfo.ROOT_DIR, dbName+".backup"));
    }

    public static Database restoreBackup(File backupFile) throws IOException,
            NotValidClassException, ClassNotFoundException {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        Type type = new TypeToken<Backup>(){}.getType();
        FileReader reader = new FileReader(backupFile);
        Backup backup = gson.fromJson(reader, type);
        reader.close();
        return new Database(backup);
    }

}
